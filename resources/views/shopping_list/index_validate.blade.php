@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Validierung</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    Bitte hier abgeschlossene Einkäufe überprüfen und validieren.

                    @include("admin.sequence")
                </div>
            </div>
        </div>
        <div class="col-md-12 mt-3">
        <h3>Listen zum validieren</h3>
        Um Einkaufsliste zu betrachten/abzuschliessen anklicken!
        <div id="accordion">
            @foreach($lists as $list)
                @include('shopping_list.expandable_list', array('list' => $list, 'status' => 'validate'))
            @endforeach
        </div>
    </div>
</div>
@endsection
