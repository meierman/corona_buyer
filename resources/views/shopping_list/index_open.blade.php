@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Offene Listen</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    Diese Listen müssen noch von Nutzern angenommen werden

                    @include("admin.sequence")
                </div>
            </div>
        </div>
        <div class="col-md-12 mt-3">
        <div id="accordion">
            @foreach($lists as $list)
                @include('shopping_list.expandable_list', array('list' => $list, 'status' => 'open_admin'))
            @endforeach
        </div>
    </div>
</div>
@endsection
