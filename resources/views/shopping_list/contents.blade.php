                      <div class="card-body">
                        Grund für den Lieferservice: 
                        @switch($list->reason)
                            @case("old")
                        Ältere Person.  
                            @break
                            @case("sick")
                        <font color="red">Person hat Krankheitssymptome (Zusätzliche Vorsicht geboten bei Übergabe!). </font> 
                            @break
                            @case("risk")
                        Person gehört einer Risikogruppe an.  
                            @break
                        @endswitch
                        <br>


                        <div class="row">

                        @if($status!='open')
                        <div class="col-md-4 mt-3">
                            <b>Kontaktinformation</b>  <br>
                            {{ $list->first_name }} {{ $list->second_name }} <br>  
                            {{ $list->street }}  <br>
                            {{ $list->zip }} {{ $list->city }} <br> 
                            @if ($list->email != NULL)
                            {{ $list->email }}  <br>
                            @endif
                            {{ $list->phone }}  <br>
                        </div>
                        @endif
                        @foreach($list->shopping_items as $shopping_item)
                            <div class="col-md-4 mt-3">
                                <b>{{ $shopping_item->item_category->title }}</b>  <br>
                                <pre>{{ str_limit($shopping_item->items, 500) }}</pre>
                            </div>
                        @endforeach
                        @if($list->comment!=NULL)
                            <div class="col-md-4 mt-3">
                                <b>Kommentar</b>  <br>
                                <pre>{{ str_limit($list->comment, 500) }}</pre>
                            </div>

                        @endif

                        @if($status=='taken')
                            <div class="col-md-4 mt-3">
                                <form action="home/confirm_shopping" enctype="multipart/form-data" id="take_list_form_{{ $list->id }}" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <h4>Einkauf erfolgreich ausgeliefert?</h4>
                                        Bezahlter Betrag: 
                                        <input type="number" step="0.01" class="form-control" name="cost" required="" value ='' placeholder="90.35">
                                    </div>
                                    <div class="form-group">
                                        Quittung(en): 
                                        <input type="file" class="form-control" name="filename[]" required="" multiple>
                                    </div>
                                    <button type="submit" class="btn btn-info" name="action" value={{ $list->id }}>Einkauf Abschliessen</button>
                                </form>
                            </div>
                        @endif
                        
                        @if($status=='validate' || $status=='be_paid' || $status=='refund' || $status=='archive' || $status=='taken_admin')
                            <div class="col-md-4 mt-3">
                                @if($list->user != NULL)
                                    <b>Ausführender Nutzer</b><br>
                                    @include('user.profile', array('user' => $list->user))
                                @else
                                    <b>Kein ausführender Nutzer gefunden (ERROR)</b><br>
                                @endif
                            </div>
                        @endif
                        @if($status=='closed' || $status=='validate' || $status=='be_paid' || $status=='refund' || $status=='archive' || $status=='open_admin' || $status=='taken_admin')
                            <div class="col-md-12 mt-3">
                                <b>Status Informationen</b><br>
                                @if($list->delivered == NULL)
                                Noch keine Quittung eingereicht<br>
                                Kosten: -<br>
                                @else
                                Quittung eingereicht: {{ $list->delivered->add(new DateInterval('PT1H'))->format('d.m.Y H:i') }}<br>
                                Kosten: {{ number_format($list->cost, 2) }}<br>
                                    @foreach($list->images as $indexKey =>$image)
                                        <a href="{{ '/images/'.$image->filename }}">Quittung #{{ $indexKey+1 }}</a><br>
                                    @endforeach
                                @endif
                                @if($list->verified == NULL)
                                Einkauf wurde noch nicht verifiziert<br>
                                @else
                                Verifiziert: {{ $list->verified->add(new DateInterval('PT1H'))->format('d.m.Y H:i') }}<br>
                                @endif
                                @if($list->refunded == NULL)
                                Einkauf wurde noch nicht zurückerstattet<br>
                                @else
                                Rückerstattung in Auftrag gegeben: {{ $list->refunded->add(new DateInterval('PT1H'))->format('d.m.Y H:i') }}<br>
                                @endif
                                @if($list->paid_time == NULL)
                                Einkauf wurde noch nicht bezahlt durch die empfangende Person<br>
                                Voraussichtliche Zahlungsmethode durch Hilfe-Empfangenden:
                                @switch($list->payment_method)
                                    @case("twint")
                                TWINT  
                                    @break
                                    @case("ebanking")
                                E-Banking  
                                    @break
                                    @case("bill")
                                Rechnung  
                                    @break
                                @endswitch

                                @else
                                Betrag bezahlt durch Empfänger: {{ number_format($list->paid, 2) }}<br>
                                Bezahlt durch Empfänger: {{ $list->paid_time->add(new DateInterval('PT1H'))->format('d.m.Y H:i') }}<br>
                                Hinweis: Allfällige Aufrundungen / Trinkgelder halten wir vorläufig zurück und werden sie später unter den Helfern aufteilen. Für Transparenz weisen wir allfällige höhere Zahlungen hier für dich ersichtlich aus. Mehr auch unter <a href="/wiki/statistiken">Statistiken</a>.<br>
                                @endif
                            </div>
                        @endif


                

                        


                        </div>
                      </div>
