<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Passwörter müssen mindestens 6 Zeichen lang sein',
    'reset' => 'Dein Passwort wurde zurückgesetzt!',
    'sent' => 'Wir haben dir einen Link zum Zurücksetzen zugesandt!',
    'token' => 'Dieser Rücksetz-Link ist ungültig.',
    'user' => "Wir können keinen Nutzer mit dieser E-Mail-Adresse finden.",

];
