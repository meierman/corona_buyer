<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WikiController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function procedure()
    {
        return view('wiki.procedure');
    }

    public function stores()
    {
        return view('wiki.stores');
    }
    public function conditions()
    {
        return view('wiki.conditions');
    }
    public function team()
    {
        return view('wiki.team');
    }
    public function statistics()
    {
        $lists_in_process = \App\shopping_list::where('delivered', NULL)->count();
        $lists_delivered = \App\shopping_list::where('taken', '<>', NULL)->count();

        $lists_paid = \App\shopping_list::where('paid_time', '<>', NULL)->count();
        $total_paid = \App\shopping_list::where('paid_time', '<>', NULL)->sum('paid');
        $total_should_paid = \App\shopping_list::where('paid_time', '<>', NULL)->sum('cost');


        $lists_unpaid = \App\shopping_list::where('paid_time', NULL)->count();
        $total_unpaid = \App\shopping_list::where('paid_time', NULL)->sum('cost');

        $data = array(
            'lists_in_process'=>$lists_in_process,
            'lists_delivered'=>$lists_delivered,
            'lists_paid'=>$lists_paid,
            'total_paid'=>$total_paid,
            'total_should_paid'=>$total_should_paid,
            'lists_unpaid'=>$lists_unpaid,
            'total_unpaid'=>$total_unpaid);

        return view('wiki.statistics')->with($data);
    }
}
