@extends('layout.basic_frame')

@section('title', 'Einkauf Erledigen während Corona Epidemie')

@section('language', 'de')

@section('body')


<?php
    $error_txt = "";
    $success = false;
    
if(isset($_POST['email'])) {
    
 
    // EDIT THE 2 LINES BELOW AS REQUIRED
    $email_to = "manuel@m2you.ch";
    $email_subject = "Neue Einnkaufsliste eingegangen";
    
    $json = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=6LegdeEUAAAAAJVQENxUo9-Vw3dnqLlELMCyiq8C&response='.$_POST['g-recaptcha-response']);
    $data = json_decode($json);    
    

    
    

    
    if(!$data->success){
        $error_txt .='Leider gab es ein Problem mit dem ReCaptcha. Bitte versuche es noch einmal!';  
    }
 
 
    // validation expected data exists
    if(!isset($_POST['first_name']) ||
        !isset($_POST['street']) ||
        !isset($_POST['email']) ||
        !isset($_POST['zip']) ||
        !isset($_POST['phone']) ||
        !isset($_POST['city'])) {
        $error_txt .='Deine Daten sind nicht vollständig, bitte fülle alle Felder aus!';       
    }
 
     
 
    $name = $_POST['name']; // required
    $email_from = $_POST['email']; // required
    $street = $_POST['street']; // required
    $zip = $_POST['zip']; // required
    $city = $_POST['city']; // required
    $comment = $_POST['comment']; // required
 
    $error_message = "";
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
 
  if(!preg_match($email_exp,$email_from)) {
    $error_txt .='Ungültige E-Mail-Adresse.';
  }
 
    $string_exp = "/^[A-Za-z .'-]+$/";
    $number_exp = "/^[A-Za-z0-9 .'-]+$/";
 
  if(!preg_match($string_exp,$name)) {
    $error_txt .='Ungültige Name.';
  }
 
  if(!preg_match($string_exp,$street)) {
    $error_txt .='Ungültige Strasse.';
  }
/*  if(!preg_match($number_exp,$zip)) {
    $error_txt .='The Zip you entered does not appear to be valid.';
  }*/
  if(!preg_match($string_exp,$city)) {
    $error_txt .='Ungültige Ortschaft.';
  }
  if(!preg_match($string_exp,$comment)) {
    $error_txt .='Ungültiger Kommentar.';
  }

  
  if(strlen($error_txt)==0){
 
    $email_message = "Siehe Formular-Daten.\n\n";
 
     
    function clean_string($string) {
      $bad = array("content-type","bcc:","to:","cc:","href");
      return htmlspecialchars(str_replace($bad,"XREPLACEDX",$string));
    }
 
     
 
    $email_message .= "Name: ".clean_string($first_name)." ".clean_string($second_name)."\n";
    $email_message .= "E-Mail: ".clean_string($email_from)."\n";
    $email_message .= "Strasse: ".clean_string($street)."\n";
    $email_message .= "Ortschaft: ".clean_string($zip)." ".clean_string($city)."\n";
    $email_message .= "Telefon: ".clean_string($phone)."\n";

    if(strlen($comment)>0){
        $email_message .= "Hinweise: ".clean_string($comment)."\n";
    }
    
 
    // create email headers
    $headers = 'From: '.$email_from."\r\n".
    'Reply-To: '.$email_from."\r\n" .
    'X-Mailer: PHP/' . phpversion();
    //@mail($email_to, $email_subject, $email_message, $headers);  
    $success = true;
    
  }
}?>
  



        <!-- Service Section -->
        <section id="service" class="service sections lightbg">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="head_title text-center">
        @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
        @endif
                            <h5>Sind Sie über 65, haben Krankheitssymptome oder gehören anderweitig zur Risikogruppe?</h5>
                            <h1>Wir möchten Ihre Einkäufe erledigen!</h1>
                            Kostenloses Angebot während Corona-Epidemie um zu helfen, dass sich gefährdete Personen nicht exponieren müssen.<br><br>
                            Angebot aktuell verfügbar in: @foreach($allowed_cities as $allowed_city) {{ $allowed_city->zip }} {{ $allowed_city->city }}, @endforeach<br><br>
                            Wir sind aktuell ein Team von {{ $user_count }} Helfern aus diesen Ortschaften. Wir helfen Ihnen sehr gerne! <a href="/wiki/wer-sind-wir">Wer wir sind</a>
                        </div><!-- End of head title -->

                        <div class="main_service_area"> 
                            <div class="single_service_area  margin-top-80">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="single_service">
                                            <h3>Ablauf eines Einkaufs</h3>
                                            <div class="separator2"></div>
                                            <ol>
                                                <li>Sie reichen Ihren Einkaufszettel hier oder telefonisch (076 769 31 00) ein</li>
                                                <li>Wir besorgen Ihre Waren nach Möglichkeit innerhalb von 2 Tagen (ansonsten melden wir uns)</li>
                                                <li>Wir liefern den Einkauf an Ihre Haustür (und halten entsprechenden Abstand)</li>
                                                <li>Sie bezahlen den Preis auf dem Kassenzettel der Ware: Per e-Banking, TWINT oder (wenn nicht anders möglich) später auf Rechnung. <b>Wir nehmen zu Ihrer Sicherheit kein Bargeld an</b>. <a href="#" c id="scrollDownBttn">Zahlungsinformationen</a></li>

                                        
                                   
                                            </ol>

                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="single_service">
                                            <h3>Hilf mit!</h3>
                                            <div class="separator2"></div>
                                            Gehörst du keiner Risikogruppe an und möchtest mithelfen?<br>
                                            <a href="/register">Registriere dich als Helfer!</a><br>
                                            Wenn du sowieso Einkaufen gehst, kannst du weiteren Personen helfen.<br><br>
                                            Wenn Nachfrage besteht und Helfer vorhanden sind, werden wir dieses Angebot auf weitere Ortschaften ausweiten.   <br>                                      
                                       </div>
                                    </div>
                                </div>
                            </div><!-- End of single service area -->

                            <div class="single_service_area margin-top-80">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="single_service">
                                            <h3>Einkaufszettel aufgeben</h3>
                                            Wenn Sie Probleme haben mit dem Einfüllen, können Sie sich auch telefonisch unter 076 769 31 00 melden
                                            <div class="separator2"></div>

                                            <form action="/shopping_lists" id="WinCookieForm" method="post">
                                                <!--<div class="col-lg-8 col-md-8 col-sm-10 col-lg-offset-2 col-md-offset-2 col-sm-offset-1">-->


                                                <?php if(strlen($error_txt)>0){ ?>
                                                <div class="alert alert-danger">
                                                  <?php echo $error_txt; ?>
                                                </div>
                                                
                                                <?php
                                                }
                                                elseif ($success){?>
                                                <div class="alert alert-success">
                                                  <strong>Thank you!</strong> Your contact info was sent to us.
                                                </div>
                                                <?php }
                                                
                                                if(!$success) {?>
                                                
                                                <div class="row">
                                                    <div class="col-sm-6 col-xs-6">
                                                        <div class="form-group">
                                                            <label>Vorname</label>
                                                            <input type="text" class="form-control" name="first_name" required="" value ='<?php echo (isset($_POST["first_name"]) ? $_POST["first_name"] : "");?>' placeholder="Max">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-xs-6">
                                                        <div class="form-group">
                                                            <label>Nachname</label>
                                                            <input type="text" class="form-control" name="second_name" value ='<?php echo (isset($_POST["second_name"]) ? $_POST["second_name"] : "");?>' placeholder="Mustermann">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>Strasse, Nr.</label>
                                                    <input type="text" class="form-control" name="street" required="" value ='<?php echo (isset($_POST["street"]) ? $_POST["street"] : "");?>' placeholder="Beispielstrasse 5">
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-4 col-xs-4">
                                                        <div class="form-group">
                                                            <label>PLZ</label>
                                                            <input type="text" class="form-control" name="zip" required="" value ='<?php echo (isset($_POST["zip"]) ? $_POST["zip"] : "");?>' placeholder="5107">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-8 col-xs-8">
                                                        <div class="form-group">
                                                            <label>Ort</label>
                                                            <input type="text" class="form-control" name="city" required="" value ='<?php echo (isset($_POST["city"]) ? $_POST["city"] : "");?>' placeholder="Schinznach-Dorf">
                                                        </div>
                                                    </div>
                                                </div>
                                                

                                                <div class="form-group">
                                                    <label>E-mail</label> (nicht zwingend, du erhältst eine Bestätigung und eine Lieferungsankündigung)
                                                    <input type="email" class="form-control" name="email" value ='<?php echo (isset($_POST["email"]) ? $_POST["email"] : "");?>' placeholder="max@bluewin.ch">
                                                </div>
                                                <div class="form-group">
                                                    <label>Telefon</label> (für allfällige Rückfragen)
                                                    <input type="text" class="form-control" name="phone" required="" value ='<?php echo (isset($_POST["phone"]) ? $_POST["phone"] : "");?>' placeholder="056 443 12 34">
                                                </div>
                                                

                                             @foreach($item_categories as $item_category)
                                                <div class="form-group">
                                                    <label>{{ $item_category->title }}</label>
                                                    <textarea class="form-control" name="item_cat_{{ $item_category->id }}"  rows="4" value ='<?php echo (isset($_POST["item_cat_{{ $item_category->id }}"]) ? $_POST["item_cat_{{ $item_category->id }}"] : "");?>' placeholder="{{ $item_category->examples }}"></textarea>
                                                </div>
                                             @endforeach


                                                <div class="form-group">
                                                    <label>Hinweise</label> (Ort zum Deponieren falls nicht zu Hause, Dringlichkeit, Spezielle Wünsche, ...)
                                                    <textarea class="form-control" name="comment"  rows="4" value ='<?php echo (isset($_POST["comment"]) ? $_POST["comment"] : "");?>' placeholder=""></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label>Grund für Service-Beanspruchung</label>  
                                                    <select name="reason" >
                                                      <option value="old" <?php echo ((isset($_POST["reason"]) && $_POST["reason"]=="old") ? "selected='selected'" : "");?>>Über 65 Jahre alt</option>
                                                      <option value="risk" <?php echo ((isset($_POST["reason"]) && $_POST["reason"]=="risk") ? "selected='selected'" : "");?>>Gehöre zu Risikogruppe</option>
                                                      <option value="sick" <?php echo ((isset($_POST["reason"]) && $_POST["reason"]=="sick") ? "selected='selected'" : "");?>>Krankheitssymptome</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Voraussichtliche Bezahlweise</label>  
                                                    <select name="payment_method" >
                                                      <option value="ebanking" <?php echo ((isset($_POST["payment_method"]) && $_POST["payment_method"]=="ebanking") ? "selected='selected'" : "");?>>e-Banking</option>
                                                      <option value="twint" <?php echo ((isset($_POST["payment_method"]) && $_POST["payment_method"]=="twint") ? "selected='selected'" : "");?>>Twint</option>
                                                      <option value="bill" <?php echo ((isset($_POST["payment_method"]) && $_POST["payment_method"]=="bill") ? "selected='selected'" : "");?>>Rechnung</option>
                                                    </select>
                                                </div>
                                                


                                                
                                                <div class="">
                                                      <div class="g-recaptcha" data-sitekey="6LegdeEUAAAAAF9P7xSqeoGYfKGuzFJlf4uU-ykp" data-callback="enableBtn" ></div>
                                                </div>
                                                <input name="language" type="hidden" value="en">
                                                <div class="">
                                                    <input type="submit" value="send" id="button1" class="btn btn-default" disabled>
                                                </div>
                                                
                                                <?php } ?>
                                                <!--</div>--> 
                                                @csrf
                                            </form>
                                           
                                            
                                            <script>
                                             function enableBtn(){
                                                document.getElementById("button1").disabled = false;
                                               }
                                            </script>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- End of single service area -->




                            <div class="single_service_area  margin-top-80">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="single_service">
                                            <h3>Organisation</h3>
                                            <div class="separator2"></div>
                                            Dieses Angebot wird organisiert durch:<br>
                                            Manuel Meier<br>
                                            Johannisweg 9<br>
                                            5107 Schinznach-Dorf<br>
                                            076 769 31 00<br>
                                            <br>
                                            Wir sind aktuell ein Team von {{ $user_count }} Helfern aus diesen Ortschaften. Wir helfen Ihnen sehr gerne! <a href="/wiki/wer-sind-wir">Wer wir sind</a>
                                            <br>
                                            
                                            <a href="/register">Registriere dich als Helfer!</a>

                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="single_service">
                                            <h3>Hinweise</h3>
                                            <div class="separator2"></div>
                                            <ul>
                                                <li>Wir unterstützen keine Hamsterkäufe</li>
                                                <li>Maximaler Einkaufsbetrag 100 CHF</li>
                                                <li>Wir können weder zeitige Lieferung noch Vollständigkeit des Einkaufs garantieren</li>
                                                <li>Falls etwas nicht exakt verfügbar ist, kaufen wir nach Möglichkeit Alternativprodukte ein</li>
                                            </ul>
                                            

                                        </div>
                                    </div>
                                </div>
                            </div><!-- End of single service area -->


                            <div class="single_service_area  margin-top-80">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="single_service" id="payment_div">
                                            <h3>Zahlungsinformationen</h3>
                                            <div class="separator2"></div>
                                            Falls Sie einen Einkauf erhalten haben, bitten wir Sie, die Ware per e-Banking oder TWINT zu bezahlen. Falls dies nicht möglich ist, werden wir nach ca. 1 Monat eine kummulierte Rechnung stellen. Bitte erwähnen Sie bei allen Überweisungen Namen und das Einkaufsdatum.<br>
                                            <ul>
                                                <li>E-Banking: 
                                                    <ul>
                                                        <li>IBAN CH87 8080 8006 7123 3936 8</li>
                                                        <li>Manuel Meier<br>Johannisweg 9<br>5107 Schinznach-Dorf</li>
                                                    </ul>
                                                , </li>
                                                <li>Twint: 077 407 94 20</li>
                                            </ul>
                                            Unser Service ist kostenlos, Sie bezahlen nur die Ware gemäss Kassenzettel. Trinkgelder werden keine erwartet. Allfällig aufgerundete Beträge werden am Ende dieser Aktion unter den Helfern aufgeteilt.
                                            

                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="single_service">
                                            <h3>Verfügbarkeit</h3>
                                            <div class="separator2"></div>
                                            Angebot aktuell Verfügbar in:
                                            <ul>
                                             @foreach($allowed_cities as $allowed_city)
                                                <li>{{ $allowed_city->zip }} {{ $allowed_city->city }}</li>
                                             @endforeach
                                             </ul>
                                             Bei entsprechenden Kapazitäten werden wir das Angebot in der Region ausweiten.

                                        </div>
                                    </div>
                                </div>
                            </div><!-- End of single service area -->                            
                        </div>
                    </div><!-- End of col-sm-12 -->
                </div><!-- End of row -->
            </div><!-- End of Container -->
        </section><!-- End of Service Section -->

@endsection