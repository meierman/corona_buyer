<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ShoppingListTakenHelper extends Mailable
{
    use Queueable, SerializesModels;

    public $shopping_list;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($shopping_list)
    {
        $this->shopping_list = $shopping_list;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Du hast eine Einkaufsliste angenommen')->markdown('mail.shopping_list-taken-helper');
    }
}
