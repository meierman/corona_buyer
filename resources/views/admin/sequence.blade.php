<br><br><b>Workflow</b>
<ol>
    <li>Listen kommen rein und sind offen (<a href="/all/offen">Offene Listen</a>)</li>
    <li>Nutzer nehmen die Listen an (<a href="/all/laufend">Laufende Einkäufe</a>)</li>
    <li>Nutzer geben Kosten ein und laden Kassenzettel-Foto hoch</li>
    <li>Lokaler Admin-Aufgabe: <a href="/all/validieren">Validierung</a> (verhindere Betrug, vergleiche Quittungs-Foto)</li>
    <li>Finanz-Aufgabe: <a href="/all/auszahlen">Auszahlung an Helfer</a></li>
    <li>Finanz-Aufgabe: <a href="/all/zahlungen_erhalten">Registrierung der Einzahlung</a></li>
    <li>Einkauf landet im <a href="/all/archiv">Archiv</a></li>
</ol>


<a href="/all/nutzer">Liste aller Nutzer (helfer)</a>
