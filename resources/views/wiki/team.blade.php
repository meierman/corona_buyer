@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">{{ __('Wer sind wir') }}</div>
                <div class="card-body">
                    <b>Diese Plattform wird nicht durch eine Organisation betrieben. Sie dient viel mehr dazu, langfristig und mit überschaubarem Aufand Helfende und Hilfesuchende zusammen zu bringen. Jeder der in einer Ortschaft Einkaufshilfe anbieten möchte, kann dies über diese Plattform tun.</b><br><br>

                    Initiator der Plattform: <br>
                    Manuel Meier<br>
                    Johannisweg 9<br>
                    5107 Schinznach-Dorf<br>
                    076 769 31 00<br>
                    <br>

                    Lokale Initiatoren:<br>
                    <ul>
                        <li><b>5082 Kaisten</b> Christian Schori, Lindengasse 10, 5082 Kaisten, 079 475 67 54, christian@tickit.ch</li>
                        <li><b>5107 Schinznach-Dorf</b> Manuel Meier, Johannisweg 9, 5107 Schinznach-Dorf, 076 769 31 00, manuel@tickit.ch</li>
                        <li><b>5108 Oberflachs</b> Manuel Meier, Johannisweg 9, 5107 Schinznach-Dorf, 076 769 31 00, manuel@tickit.ch</li>
                        <li><b>5112 Thalheim</b> Katja Sommerhalder, Hegi 360, 5112 Thalheim, katja@tickit.ch</li>
                        <li><b>5200 Brugg</b> Samuel Tiefenauer, Altenburgerstrasse 47, 5200 Brugg, samuel@tickit.ch</li>
                    </ul>

                    <br>
                    Möchtest du Einkaufshilfe in deinem Dorf/Quartier über diese Plattform anbieten? Melde dich bei Manuel (076 769 31 00, manuel@tickit.ch). Das Angebot ist absolut kostenlos.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
