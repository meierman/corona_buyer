@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Einzahlungen</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    Bitte hier Einzahlungen eintragen.

                    @include("admin.sequence")

                </div>
            </div>
        </div>
        <div class="col-md-12 mt-3">
        <h3>Offene Rechnungen</h3>
        Um Einkaufsliste zu betrachten anklicken!
        <div id="accordion">
            @foreach($lists as $list)
                @include('shopping_list.expandable_list', array('list' => $list, 'status' => 'be_paid'))
            @endforeach
        </div>
    </div>
</div>
@endsection
