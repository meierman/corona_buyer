@component('mail::message')

# Jemand kümmert sich um Ihren Einkauf

Soeben hat sich ein Helfer bereit erklärt, sich um Ihren Einkauf zu kümmern. Kontaktdaten des Helfers für Fragen/Änderungswünsche/Probleme:<br><br>

{{ $shopping_list->user->name }} {{ $shopping_list->user->second_name }}<br>
{{ $shopping_list->user->email }}<br>
{{ $shopping_list->user->phone }}<br>

<br>
Bitte beachten Sie, dass wir keine Vollständigkeit der Einkäufe garantieren können. <br>
<b>Bitte bezahlen Sie die Einkäufe nicht an der Haustür und halten Sie jederzeit Abstand.</b> Zahlungsinformationen finden Sie auf unserer Website, ein (oder mehrere) Kassenzettel sollte der Ware beiliegen.

Herzliche Grüsse,<br>
Manuel von tickit.ch
@endcomponent