<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/welcome', function () {
    return view('welcome');
});
/*
Route::get('/', function () {
	
    return view('landing_page');
});*/
Route::get('/', 'ShoppingListController@create');

Route::post('/shopping_lists', 'ShoppingListController@store');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/home/take_shopping_list', 'HomeController@link_list_user');
Route::post('/home/confirm_shopping', 'HomeController@confirm_shopping');
Route::get('/home/shopping_list_print/{id}', 'HomeController@print_view');
//Route::get('/home/take_shopping_list', 'HomeController@link_list_user');
Route::get('/wiki/ablauf', 'WikiController@procedure')->name('home');
Route::get('/wiki/laeden', 'WikiController@stores')->name('home');
Route::get('/wiki/nutzungsbedingungen', 'WikiController@conditions')->name('home');
Route::get('/wiki/wer-sind-wir', 'WikiController@team')->name('home');
Route::get('/wiki/statistiken', 'WikiController@statistics');


Route::get('/all/validieren', 'AdminController@list_to_validate');
Route::get('/all/auszahlen', 'AdminController@list_to_refund');
Route::get('/all/zahlungen_erhalten', 'AdminController@list_to_be_paid');
Route::get('/all/archiv', 'AdminController@list_archive');
Route::get('/all/offen', 'AdminController@list_open');
Route::get('/all/laufend', 'AdminController@list_taken');
Route::get('/all/nutzer', 'AdminController@list_users');

Route::post('/all/validieren', 'AdminController@validate_list');
Route::post('/all/auszahlen', 'AdminController@refund_list');
Route::post('/all/zahlungen_erhalten', 'AdminController@be_paid_list');


Route::get('/invoice', 'ShoppingListController@getpdf');
Route::get('all/invoice/{id}', 'ShoppingListController@getpdf');
