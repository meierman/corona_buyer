@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">{{ __('Statistiken') }}</div>
                <div class="card-body">
                            
                    Total Einkäufe: {{ $lists_in_process+$lists_delivered }}<br>
                    Davon aktuell laufend/offen: {{ $lists_in_process }}<br>
                    Ausgeliefert: {{ $lists_delivered }}<br><br>

                    Offene Listen von dir: {{ \Auth::user()->processing_lists_count() }}<br>
                    Abgeschlossene Listen von dir: {{ \Auth::user()->finished_lists_count() }}<br><br>

                    Listen bezahlt durch Empfänger: {{ $lists_paid }} ({{ number_format($total_paid, 2) }} CHF)<br>
                    Bezahlung durch Empfänger ausstehend: {{ $lists_unpaid }} ({{ number_format($total_unpaid, 2) }} CHF)<br>
                    Total Trinkgeld: {{ number_format($total_paid-$total_should_paid, 2) }} CHF<br>
                    Dein voraussichtliches Trinkgeld: {{ number_format(($total_paid-$total_should_paid)/($lists_in_process+$lists_delivered)*\Auth::user()->shopping_lists->count(), 2) }} CHF<br>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
