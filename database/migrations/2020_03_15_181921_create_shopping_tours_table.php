<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShoppingToursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shopping_tours', function (Blueprint $table) {
            $table->increments('id');
            $table->float('km')->default(0);
            $table->timestamp('billing_approval')->nullable();
            $table->timestamp('billing_finished')->nullable();
            $table->timestamps();

            //relations
            $table->unsignedInteger('user_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shopping_tours');
    }
}
