<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use Carbon\Carbon;
use DateInterval;
class ShoppingListController extends Controller
{
    // GUI to create
    public function create()
    {
    	$allowed_cities = \App\allowed_cities::orderBy('zip', 'asc')->get();
    	$item_categories = \App\item_category::all();
        $user_count = \App\User::count();
    	$data = array('allowed_cities'=>$allowed_cities, 'item_categories'=>$item_categories, 'user_count'=>$user_count);
    	return view('landing_page')->with($data);
    }

    // Create Object
    public function store(){
    	$shopping_list = new \App\shopping_list();
    	$shopping_list->first_name = request('first_name');
    	$shopping_list->second_name = request('second_name');
    	$shopping_list->street = request('street');
    	$shopping_list->zip = request('zip');
    	$shopping_list->city = request('city');
    	$shopping_list->email = request('email');
    	$shopping_list->phone = request('phone');
    	$shopping_list->comment = request('comment');
        $shopping_list->reason = request('reason');
        $shopping_list->payment_method = request('payment_method');
    	$shopping_list->save();


    	// Create Items
    	$item_categories = \App\item_category::all();
    	foreach($item_categories as $item_category){
    		if(strlen(request('item_cat_'.$item_category->id))>1){
    			$shopping_item = new \App\shopping_item();
    			$shopping_item->items = request('item_cat_'.$item_category->id);
    			$shopping_item->item_category_id = $item_category->id;
    			$shopping_list->shopping_items()->save($shopping_item);
    		}
    	}


        if($shopping_list->email != NULL){
            \Mail::to($shopping_list->email)->cc('info@tickit.ch')->send(
                new \App\Mail\ShoppingListCreated($shopping_list)
            );

        }
        else{
            \Mail::to('info@tickit.ch')->send(
                new \App\Mail\ShoppingListCreated($shopping_list)
            );
        }

    	//\Mail::to('team@kooki.ch')->send(
    	//	new \App\Mail\NewsletterCreated($newsletter)
    	//);

    	return redirect('/')->with('status', 'Einkaufsliste erfolgreich abgespeichert! Wir kümmern uns möglichst schnell darum. Falls du eine E-Mail-Adresse angegeben hast, kriegst du dort Status Informationen.');;

    }


    public function getPdf($id/*$type = 'stream'*/)
    {
        $list = \App\shopping_list::where('id', $id)->first();
        $lists = \App\shopping_list::where('paid_time', NULL)->where('zip', $list->zip)->where('first_name', $list->first_name)->where('second_name', $list->second_name)->where('user_id', '<>' , NULL)->where('delivered', '<>' , NULL)->get();

        //dd($lists[0]->first_name);

        $pdf = PDF::loadView('pdf.invoice-pdf', compact(['list', 'lists']));

        /*if ($type == 'stream') {
            return $pdf->stream('invoice.pdf');
        }*/

        //if ($type == 'download') {
            return $pdf->download('rechnung_'.$list->first_name.$list->second_name.'.pdf');
        //}
    }
}
