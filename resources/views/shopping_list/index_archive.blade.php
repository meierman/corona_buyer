@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Archiv</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    Alle komplett abgeschlossenen Einkäufe sind hier zu finden

                    @include("admin.sequence")
                </div>
            </div>
        </div>
        <div class="col-md-12 mt-3">
            <div id="accordion">
                @foreach($lists as $list)
                    @include('shopping_list.expandable_list', array('list' => $list, 'status' => 'archive'))
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection
