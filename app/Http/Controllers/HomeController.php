<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use DateInterval;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = \Auth::user();
        $open_lists = \App\shopping_list::where('taken', NULL)->orderBy('zip', 'asc')->get();
        $my_open_lists =  $user->shopping_lists->where('delivered', NULL); //\App\shopping_list::where('taken', '<>', NULL/*'id', \Auth::user()->id*/)->get();
        $my_closed_lists =  $user->shopping_lists->where('delivered', '<>', NULL); //\App\shopping_list::where('taken', '<>', NULL/*'id', \Auth::user()->id*/)->get();
        //$item_categories = \App\item_category::all();
        $data = array('open_lists'=>$open_lists, 'my_open_lists'=>$my_open_lists, 'my_closed_lists'=>$my_closed_lists);

        //dd($data);

        //$users = App\User::where()->all();
        return view('home')->with($data);
    }


    // User accepts List
    public function confirm_shopping(Request $request)
    {
        $user = \Auth::user();
        $open_list = \App\shopping_list::where('id', $request->input('action'))->first();
        if($open_list->user_id == $user->id){
            $images=array();
            if($files=$request->file('filename')){
                foreach($files as $file){
                    $name=$file->getClientOriginalName();
                    $base_name = pathinfo($name, PATHINFO_FILENAME);
                    $extension = pathinfo($name, PATHINFO_EXTENSION);
                    $base_name .= Carbon::now()->add(new DateInterval('PT1H'))->format('_d_m_Y_H_i_s');
                    $base_name .= '_'.str_random(5);
                    $name = $base_name.'.'.$extension;
                    $file->move(public_path().'/images/', $name);
                    $images[]=$name;

                    $img_obj = new \App\image();
                    $img_obj->filename = $name;
                    $open_list->images()->save($img_obj);                
                }
            }
            $open_list->delivered = Carbon::now();
            $open_list->cost = $request['cost'];
            $open_list->save();
        }
        return redirect('/home');
    }

    // User confirms goods are bought
    public function link_list_user(Request $request)
    {
        $user = \Auth::user();
         //dd($request->all());
        $open_list = \App\shopping_list::where('id', $request->input('action'))->first();
        if($open_list->user_id == NULL){
            $open_list->fresh();
            $open_list->user_id = $user->id;
            $open_list->taken = Carbon::now();
            $open_list->save();

            if($open_list->email != NULL){
                \Mail::to($open_list->email)->cc('info@tickit.ch')->send(
                    new \App\Mail\ShoppingListTakenClient($open_list)
                );
            }
            \Mail::to($open_list->user->email)->cc('info@tickit.ch')->send(
                new \App\Mail\ShoppingListTakenHelper($open_list)
            );

        }
        return redirect('/home');
    }



    public function print_view($id)
    {
        $list = \App\shopping_list::where('id', $id)->first();
        $data = array('list'=>$list);
        if($list->user_id == \Auth::user()->id){
            return view('shopping_list.print_view')->with($data);
        }
        return redirect('/home');
    }

}
