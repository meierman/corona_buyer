@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Übersicht</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    Besten Dank fürs Reinschauen. Falls es offene Listen gibt, werden diese hier angezeigt.<br>
                    Schau vor deiner nächsten Einkaufstour wieder vorbei! Bei Fragen/Problemen: info@tickit.ch oder 076 769 31 00<br>
                    <ul>
                        <li><a href="/wiki/ablauf">Ablauf eines Einkaufs als Helfer</a></li>
                        <li><a href="/wiki/nutzungsbedingungen">Nutzungsbedingungen für Helfer</a></li>
                        <li>Den Ort zum Einkaufen darfst du selbst wählen (auch wenn dort einzelne gewünschte Produkte nicht verfügbar sind). Wir empfehlen das Unterstützen von lokalen betroffenen Händlern. Hier findest du eine <a href="/wiki/laeden">Liste von Händlern</a>, die sich bereits aktiv bei uns gemeldet haben.</li>
                        <li>Falls du in deinem Quartier/Dorf Flyer verteilen möchtest, kannst du dich bei Manuel melden (Kontakt oben)</li>
                        <li><a href="/wiki/statistiken">Statistiken</a></li>

                </div>
            </div>
        </div>
        <div class="col-md-12 mt-3">
        @if(count($my_open_lists)>0)
        <h3>Deine offenen Einkaufslisten</h3>
        Um Einkaufsliste zu betrachten/abzuschliessen anklicken!
        <div id="accordion">
            @foreach($my_open_lists as $list)
                @include('shopping_list.expandable_list', array('list' => $list, 'status' => 'taken'))
            @endforeach
        </div>
        @endif

        <br><br>
        <h3>Offene Einkaufslisten</h3>
        @if(count($open_lists)==0)
            Aktuell gibt es keine offenen Einkaufslisten. Schau später wieder vorbei!
        @else
        Um Einkaufsliste zu betrachten anklicken! Kontaktdaten sind hier versteckt, bis du eine Liste annimmst.
        <div id="accordion">
            @foreach($open_lists as $list)
                  @include('shopping_list.expandable_list', array('list' => $list, 'status' => 'open'))
            @endforeach
        </div>
        @endif

        @if(count($my_closed_lists)>0)
        <br><br>
        <h3>Deine geschlossenen Einkaufslisten</h3>
        Um Einkaufsliste zu betrachten/abzuschliessen anklicken!
        <div id="accordion">
            @foreach($my_closed_lists as $list)
                  @include('shopping_list.expandable_list', array('list' => $list, 'status' => 'closed'))
            @endforeach
        </div>
        @endif
    </div>
</div>
@endsection
