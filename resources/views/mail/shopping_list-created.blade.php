@component('mail::message')

# Ihre Einkaufsliste

Bitte überprüfen Sie alle Angaben noch einmal. Bei Fehlern können Sie sich unter info@tickit.ch oder 076 769 31 00 melden.

{{ $shopping_list->first_name }} {{ $shopping_list->second_name }}   
{{ $shopping_list->street }}  
{{ $shopping_list->zip }} {{ $shopping_list->city }}  
@if ($shopping_list->email != NULL)
{{ $shopping_list->email }}  
@endif
{{ $shopping_list->phone }}  


@switch($shopping_list->reason)
	@case("old")
Grund: Ältere Person  
	@break
	@case("sick")
Grund: **Person hat Krankheitssymptome** (Zusätzliche Vorsicht geboten bei Übergabe!)  
	@break
	@case("risk")
Grund: Person gehört einer Risikogruppe an  
	@break
@endswitch
@switch($shopping_list->payment_method)
	@case("twint")
Voraussichtliche Zahlungsmethode: TWINT  
	@break
	@case("ebanking")
Voraussichtliche Zahlungsmethode: E-Banking  
	@break
	@case("bill")
Voraussichtliche Zahlungsmethode: Rechnung  
	@break
@endswitch

@if ($shopping_list->comment!=NULL)
**Kommentar des Auftraggebers:**  
{{ $shopping_list->comment }} 
@endif


@foreach($shopping_list->shopping_items as $shopping_item)
**{{ $shopping_item->item_category->title }}**  
{{ str_limit($shopping_item->items, 500) }}

@endforeach
@endcomponent