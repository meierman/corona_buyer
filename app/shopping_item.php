<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class shopping_item extends Model
{
	// allow mass assignment
    protected $guarded = [];

    // Relationships
    public function shopping_list()
    {
    	return $this->belongsTo(shopping_list::class);
    }
    public function item_category()
    {
    	return $this->belongsTo(item_category::class);
    }
}
