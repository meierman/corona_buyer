@component('mail::message')

# Hallo {{ $user->name }} 

Vielen Dank, dass du bereit bist, gefährdete Menschen mit Einkäufen zu unterstützen. Wir empfehlen, dass du vor jeder Einkaufstour dich kurz einloggst unter [tickit.ch/login](https://tickit.ch/login) und schaust, ob es offene Einkaufslisten gibt, die du übernehmen könntest.


Bitte beachte, dass die Website noch im Aufbau ist und möglicherweise Fehler auftreten. Diese kannst du gerne melden unter: info@tickit.ch.


Liebe Grüsse,

Manuel

@endcomponent