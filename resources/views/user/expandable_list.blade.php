<div class="card">
  <div class="card-header" id="headingOne">
      <div class="row">

        {{-- Different Headers --}}
          <div class="col-md-5">
            <h5 class="mb-0">
              <a class="collapsed" data-toggle="collapse" href="#collapse_{{ $user->id }}" aria-expanded="false" aria-controls="collapse_{{ $user->id }}">
                {{ $user->zip }}, {{ str_limit($user->name, 10) }} {{ str_limit($user->second_name, 10) }}
              </a>
            </h5>
          </div>
          <div class="col-md-5">
            Anzahl Einkaufslisten: {{ count($user->shopping_lists) }}
          </div>
      </div>

  </div>

  <div id="collapse_{{ $user->id }}" class="collapse" aria-labelledby="heading_{{ $user->id }}" data-parent="#accordion">
    <div class="card-body">
      <div class="row">
        <div class="col-md-12 mt-3">
                @include('user.profile', array('user' => $user))
                Registriert: {{ $user->created_at->add(new DateInterval('PT1H'))->format('d.m.Y H:i') }} 
        </div>
      </div>
    </div>
  </div>
</div>