@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Nutzer</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @include("admin.sequence")
                </div>
            </div>
        </div>
        <div class="col-md-12 mt-3">
        <div id="accordion">
            @foreach($users as $user)
                @include('user.expandable_list', array('user' => $user))
            @endforeach
        </div>
    </div>
</div>
@endsection
