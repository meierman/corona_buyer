<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class shopping_list extends Model
{
	// allow mass assignment
    //protected $guarded = [];

    // Relationships

    protected $dates = ['verified', 'paid_time', 'taken', 'delivered', 'refunded'];

    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id', 'id');
    }
    public function shopping_items()
    {
        return $this->hasMany(shopping_item::class);
    }
    public function images()
    {
        return $this->hasMany(image::class);
    }
}
