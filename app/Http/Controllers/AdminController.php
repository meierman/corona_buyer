<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

class AdminController extends Controller
{

    protected $admin_ids = array(1, 9999);


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }



    // User accepts List
    public function list_open()
    {
        $user = \Auth::user();
        if(in_array($user->id, $this->admin_ids)){
            $lists = \App\shopping_list::where('taken', NULL)->get();
            $data = array('lists'=>$lists);
            return view('shopping_list.index_open')->with($data);
            //dd($lists);
        }
        return "fail";
    }
    // User accepts List
    public function list_taken()
    {
        $user = \Auth::user();
        if(in_array($user->id, $this->admin_ids)){
            $lists = \App\shopping_list::where('taken', '<>', NULL)->where('delivered', NULL)->get();
            $data = array('lists'=>$lists);
            return view('shopping_list.index_taken')->with($data);
            //dd($lists);
        }
        return "fail";
    }

    // User accepts List
    public function list_to_validate()
    {
        $user = \Auth::user();
        if(in_array($user->id, $this->admin_ids)){
            $lists = \App\shopping_list::where('delivered', '<>', NULL)->where('verified', NULL)->get();
            $data = array('lists'=>$lists);
            return view('shopping_list.index_validate')->with($data);
            //dd($lists);
        }
        return "fail";
    }
    // User accepts List
    public function list_to_refund()
    {
        $user = \Auth::user();
        if(in_array($user->id, $this->admin_ids)){
            $lists = \App\shopping_list::where('verified', '<>', NULL)->where('refunded', NULL)->get();
            $data = array('lists'=>$lists);
            return view('shopping_list.index_refund')->with($data);
            //dd($lists);
        }
        return "fail";
    }
    // User accepts List
    public function list_to_be_paid()
    {
        $user = \Auth::user();
        if(in_array($user->id, $this->admin_ids)){
            $lists = \App\shopping_list::where('delivered', '<>', NULL)->where('paid_time', NULL)->get();
            $data = array('lists'=>$lists);
            return view('shopping_list.index_be_paid')->with($data);
            //dd($lists);
        }
        return "fail";
    }
    // User accepts List
    public function list_archive()
    {
        $user = \Auth::user();
        if(in_array($user->id, $this->admin_ids)){
            $lists = \App\shopping_list::where('refunded', '<>', NULL)->where('paid_time', '<>', NULL)->get();
            $data = array('lists'=>$lists);
            return view('shopping_list.index_archive')->with($data);
            //dd($lists);
        }
        return "fail";
    }
    // User accepts List
    public function list_users()
    {
        $user = \Auth::user();
        if(in_array($user->id, $this->admin_ids)){
            $all_users = \App\User::get();
            $data = array('users'=>$all_users);
            return view('user.index_all')->with($data);
            //dd($lists);
        }
        return "fail";
    }


    // User accepts List
    public function validate_list(Request $request)
    {
        $user = \Auth::user();
        if(in_array($user->id, $this->admin_ids)){
            $list = \App\shopping_list::where('id', $request->input('action'))->first();
            $list->verified = Carbon::now();
            $list->save();
            return redirect('/all/validieren');
        }
        return "fail";
    }
    // User accepts List
    public function refund_list(Request $request)
    {
        $user = \Auth::user();
        if(in_array($user->id, $this->admin_ids)){
            $list = \App\shopping_list::where('id', $request->input('action'))->first();
            $list->refunded = Carbon::now();
            $list->save();
            return redirect('/all/auszahlen');
        }
        return "fail";
    }
    // User accepts List
    public function be_paid_list(Request $request)
    {
        $user = \Auth::user();
        if(in_array($user->id, $this->admin_ids)){
            $list = \App\shopping_list::where('id', $request->input('action'))->first();
            $list->paid_time = Carbon::now();
            $list->paid = $request->input('paid');
            $list->save();
            return redirect('/all/zahlungen_erhalten');
        }
        return "fail";
    }





    // User confirms goods are bought
    public function link_list_user(Request $request)
    {
        $user = \Auth::user();
         //dd($request->all());
        $open_list = \App\shopping_list::where('delivered', '<>', NULL)->where('verified', NULL)->get();
        if($open_list->user_id == NULL){
            $open_list->fresh();
            $open_list->user_id = $user->id;
            $open_list->taken = Carbon::now();
            $open_list->save();
        }
        return redirect('/home');
    }



    public function print_view($id)
    {
        $list = \App\shopping_list::where('id', $id)->first();
        $data = array('list'=>$list);
        if($list->user_id == \Auth::user()->id){
            return view('shopping_list.print_view')->with($data);
        }
        return redirect('/home');
    }

}
