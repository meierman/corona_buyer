<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class shopping_tour extends Model
{
	// allow mass assignment
    protected $guarded = [];

    // Relationships

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
    public function shopping_lists()
    {
    	return $this->hasMany(shopping_list::class);
    }
    //
}
