<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class item_category extends Model
{
	// allow mass assignment
    protected $guarded = [];

    // Relationships
    public function shopping_item()
    {
    	return $this->hasMany(shopping_item::class);
    }
    //
}
