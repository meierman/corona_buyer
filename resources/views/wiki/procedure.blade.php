@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Ablauf eines Einkaufs (Informationen für Helfende)') }}</div>
                <div class="card-body">
                    <b>Als Helfender erklärst du dich mit folgendem Ablauf für Einkäufe einverstanden:</b><br>
                        <ol>
                            <li>Du loggst dich ein unter tickit.ch/login bevor du einkaufen gehst</li>
                            <li>Falls offene Einkaufslisten vorhanden sind, kannst du eine oder mehrere anwählen, die du übernehmen möchtest.</li>
                            <li>Du druckst die Einkaufsliste(n) aus oder schreibst sie ab.</li>
                            <li>Im Laden legst du die Produkte jeder Einkaufsliste in eine separate Tasche (Papiertaschen von der Kasse) und bezahlst jede separat.</li>
                            <li>Du machst ein Foto des Kassenzettels und legst ihn dann zu den Einkäufen</li>
                            <li>Du deponierst die Einkäufe an der angegebenen Adresse, klingelst und nimmst min. 2m Abstand von der Tür. Falls die Person nicht zuhause ist, deponierst du die Tasche in Tür-Nähe.</li>
                            <li>Du nimmst von der Person kein Bargeld entgegen - auch kein Trinkgeld. Die Person kann die Einkäufe gemäss der Website über e-Banking oder Twint bezahlen. Falls dies nicht möglich ist (ältere Personen), werden wir später eine Rechnung stellen. <b>Halte jederzeit einen Abstand von über 2 Meter - auch wenn es sich komisch anfühlt. </b></li>
                            <li>Zuhause am Computer oder direkt am Telefon loggst du dich wiederum unter tickit.ch/login ein, gibst für jede Einkaufsliste den Kaufbetrag an und lädst das jeweilige Foto hoch</li>
                            <li>Wir überweisen dir den Kaufbetrag so schnell wie möglich.</li>
                            <li>Bei Fragen, Problemen oder Unklarheiten meldest du dich unter info@tickit.ch oder 076 769 31 00.</li>
                        </ol>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
