            <div class="card">
                <div class="card-header">{{ __('Informationen für Helfende') }}</div>
                <div class="card-body">
                    <b>Bitte lies diese Informationen Aufmerksam durch. Als Helfer erklärst du dich mit folgenden Punkten einverstanden:</b>
                    <ul>
                        <li>Du hast den <a href="/wiki/ablauf">Ablauf eines Einkaufs als Helfer</a> studiert und bist damit einverstanden.</li>
                        <li>Du bist dir bewusst, dass du mit Leuten aus der Risikogruppe zu tun haben wirst und hältst jederzeit mindestens 2 Meter Abstand.</li>
                        <li>Bei Fragen, Problemen oder Unklarheiten meldest du dich unter info@tickit.ch oder 076 769 31 00.</li>
                        <li>Deine Kontaktdaten werden vertraulich behandelt. Wenn du eine Einkaufsliste übernimmst, erhält die entsprechende Person (sofern sie eine Mail-Adresse angegeben hat) deine Namen, Telefonnummer und Mail-Adresse.</li>
                        <li>Diese Plattform befindet sich noch im Aufbau. Noch sind nicht alle Funktionalitäten in Betrieb. Bei Fehlern kontaktierst du Manuel Meier über obige Kontaktinformationen.</li>
                        <li>So sieht die Liste mit den Einkäufen aus: <a href="/img/uebersicht.png">Übersicht Einkaufslisten</a>, <a href="/img/einkauf.png">Detail von angenommener Einkaufsliste</a> (Dies sind Beispielfotos, die entsprechende Seite findest du <a href="/home">hier</a>, wenn du einen Account hast.</li>
                    </ul>

                </div>
            </div>