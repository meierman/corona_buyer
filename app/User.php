<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'second_name', 'street', 'zip', 'city', 'email', 'phone', 'twint_phone', 'iban', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    // Relationships
    public function shopping_lists()
    {
        return $this->hasMany('App\shopping_list');
        //return $this->hasMany(shopping_list::class);//, 'user_id', 'id'/*shopping_list::class*/);
    }

    // Relationships
    public function finished_lists_count()
    {
        return $this->hasMany('App\shopping_list')->where('delivered', '<>', NULL)->count();
        //return $this->hasMany(shopping_list::class);//, 'user_id', 'id'/*shopping_list::class*/);
    }
    // Relationships
    public function processing_lists_count()
    {
        return $this->hasMany('App\shopping_list')->where('delivered', NULL)->count();
        //return $this->hasMany(shopping_list::class);//, 'user_id', 'id'/*shopping_list::class*/);
    }
}