<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShoppingListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shopping_lists', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('second_name')->nullable();
            $table->string('street');
            $table->unsignedInteger('zip');
            $table->string('city');
            $table->enum('reason', ['old', 'sick', 'risk']);
            $table->enum('payment_method', ['ebanking', 'twint', 'bill']);
            $table->text('comment')->nullable();
            $table->float('cost')->nullable();
            $table->float('paid')->nullable();
            $table->timestamp('taken')->nullable();
            $table->timestamp('paid_time')->nullable();
            $table->timestamp('refunded')->nullable();
            $table->timestamp('delivered')->nullable();
            $table->timestamp('verified')->nullable();
            $table->timestamps();

            //relations
            $table->unsignedInteger('user_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shopping_lists');
    }
}
