<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
    
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-160807405-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-160807405-1');
{{--  @guest
    gtag('set', {'user_id': '0'});
@else
    gtag('set', {'user_id': '{{ \Auth::user()->id }}'});
@endguest--}}

@auth
    gtag('config', 'UA-160807405-1', {
      'user_id': '{{ \Auth::user()->id }}'
    });
@endauth

</script>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>@yield('title', 'Kooki')</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
        <link rel="manifest" href="/site.webmanifest">
        <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="theme-color" content="#ffffff">


        
        
        <!--Google fonts links-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">

        <script src="https://www.google.com/recaptcha/api.js?hl=@yield('language')" async defer></script>
        
        <link rel="stylesheet" href="/css/fonticons.css">
        <link rel="stylesheet" href="/css/slider-pro.css">
        <!--<link rel="stylesheet" href="/css/stylesheet.css">-->
        <link rel="stylesheet" href="/css/font-awesome.min.css">
        <link rel="stylesheet" href="/css/bootstrap.min.css">


        <!--For Plugins external css-->
        <link rel="stylesheet" href="/css/plugins.css" />

        <!--Theme custom css -->
        <link rel="stylesheet" href="/css/style.css">

        <!--Theme Responsive css-->
        <link rel="stylesheet" href="/css/responsive.css" />

        <script src="/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
        
         <!--Cookie Alert css-->
        <link rel="stylesheet" href="/css/cookiealert.css">



    </head>
    <body data-spy="scroll" data-target=".navbar-collapse">

        <div class="linklist">
                        <!-- Authentication Links -->
                        @guest
                                <a class="nav-link text-right" href="{{ route('login') }}">{{ __(' Login (Helfer)') }}</a>
                        @else
                                <a class="nav-link text-right" href="/home">{{ __(' Helfer-Bereich') }}</a>
                        @endguest
        </div>



    @yield('body')






        <!-- footer Section -->
        <footer id="footer" class="footer">
            <div class="container">
                <div class="main_footer">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="copyright_text text-center">
                                <p class=" wow fadeInRight" data-wow-duration="1s"><i class="fa fa-map-marker"></i> <span>Johannisweg 9, 5107 Schinznach-Dorf</span>&emsp;&emsp;<i class="fa fa-envelope-o"></i> <span>info@tickit.ch</span>&emsp;&emsp; &#9400; 2020. All Rights Reserved</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- End of container -->
        </footer><!-- End of footer -->



        <!-- START Bootstrap-Cookie-Alert -->
<div class="alert text-center cookiealert" role="alert">
    Diese Website verwendet Cookies <a href="https://cookiesandyou.com/" target="_blank">Mehr Infos</a>
    <button type="button" class="btn btn-primary btn-sm acceptcookies" aria-label="Close">
        Zustimmen
    </button>
</div>
<!-- END Bootstrap-Cookie-Alert -->
        
        
        <script src="https://cdn.jsdelivr.net/gh/Wruczek/Bootstrap-Cookie-Alert@gh-pages/cookiealert.js"></script>
        
        
        <!-- START SCROLL TO TOP  -->



        <script src="/js/vendor/jquery-1.11.2.min.js"></script>
        <script src="/js/vendor/bootstrap.min.js"></script>

        <script src="/js/jquery.easing.1.3.js"></script>
        <script src="/js/masonry/masonry.min.js"></script>
        <script type="text/javascript">
            $('.mixcontent').masonry();
        </script>

        <script src="/js/jquery.sliderPro.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function ($) {
                $('#example3').sliderPro({
                    width: 960,
                    height: 200,
                    fade: true,
                    arrows: false,
                    buttons: true,
                    fullScreen: false,
                    shuffle: true,
                    smallSize: 500,
                    mediumSize: 1000,
                    largeSize: 3000,
                    thumbnailArrows: true,
                    autoplay: false,
                    thumbnailsContainerSize: 960

                });
            });
            
            
            $("#scrollDownBttn").click(function(){
                $('html, body').animate({
                    scrollTop: $("#payment_div").offset().top
                }, 2000);
            });
                <?php    
        if(isset($_POST['email'])) { ?>
        $(function() {
            $('html, body').animate({
                scrollTop: $("#payment_div").offset().top
            }, 2000);
         });
        <?php }?>
        </script>
        <script src="/js/plugins.js"></script>
        <script src="/js/main.js"></script>
    </body>
</html>
