<div class="card">
  <div class="card-header" id="headingOne">
      <div class="row">

        {{-- Different Headers --}}
        @if($status!='be_paid' && $status!='refund' && $status!='validate' && $status!='archive')
          <div class="col-md-5">
            <h5 class="mb-0">
              <a class="collapsed" data-toggle="collapse" href="#collapse_{{ $list->id }}" aria-expanded="false" aria-controls="collapse_{{ $list->id }}">
                {{ $list->zip }} {{ str_limit($list->city, 15) }},  {{ str_limit(preg_replace('/[0-9,]+/', '', $list->street), 15) }} 
              </a>
            </h5>
          </div>
          <div class="col-md-3">
              <div class="mb-0">
                Eingang: {{ $list->created_at->add(new DateInterval('PT1H'))->format('d.m.Y H:i') }} 
            </div>
          </div>
        @elseif($status=='validate' || $status=='archive' || $status=='open_admin' || $status=='taken_admin')
          <div class="col-md-5">
            <h5 class="mb-0">
              <a class="collapsed" data-toggle="collapse" href="#collapse_{{ $list->id }}" aria-expanded="false" aria-controls="collapse_{{ $list->id }}">
                {{ $list->zip }}, {{ str_limit($list->first_name, 10) }} {{ str_limit($list->second_name, 10) }}
              </a>
              Helfer: {{ str_limit($list->user->name, 10) }} {{ str_limit($list->user->second_name, 10) }}
            </h5>
          </div>
          <div class="col-md-3">
              <div class="mb-0">
                Eingang: {{ $list->created_at->add(new DateInterval('PT1H'))->format('d.m.Y H:i') }} 
              </div>
          </div>
        @elseif($status=='be_paid')
          <div class="col-md-3">
            <h5 class="mb-0">
              <a class="collapsed" data-toggle="collapse" href="#collapse_{{ $list->id }}" aria-expanded="false" aria-controls="collapse_{{ $list->id }}">
                {{ str_limit($list->first_name, 10) }} {{ str_limit($list->second_name, 10) }}
              </a>
            </h5>
          </div>
          <div class="col-md-3">
              <div class="mb-0">
                Kosten: {{ $list->cost }} 
                                @switch($list->payment_method)
                                    @case("twint")
                                (TWINT)
                                    @break
                                    @case("ebanking")
                                (E-Banking)  
                                    @break
                                    @case("bill")
                                (Rechnung)  
                                    @break
                                @endswitch
            </div>
          </div>
          <div class="col-md-3">
              <div class="mb-0">
                Eingang: {{ $list->created_at->add(new DateInterval('PT1H'))->format('d.m.Y H:i') }} 
            </div>
          </div>
        @elseif($status=='refund')
          <div class="col-md-3">
            <h5 class="mb-0">
              <a class="collapsed" data-toggle="collapse" href="#collapse_{{ $list->id }}" aria-expanded="false" aria-controls="collapse_{{ $list->id }}">
                {{ str_limit($list->user->name, 10) }} {{ str_limit($list->user->second_name, 10) }}, {{ $list->user->zip }} {{ str_limit($list->user->city, 10) }}
              </a>
            </h5>
          </div>
          <div class="col-md-2">
              <div class="mb-0">
                Kosten: {{ $list->cost }} 
            </div>
          </div>
          <div class="col-md-3">
              <div class="mb-0">
                @if($list->user->iban!=NULL)
                IBAN: {{ $list->user->iban }}<br>
                @endif
                @if($list->user->twint_phone!=NULL)
                Twint: {{ $list->user->twint_phone }}
                @endif 
                @if($list->user->twint_phone==NULL && $list->user->iban==NULL)
                Keine Bankverbindung angegeben! Anrufen!
                @endif 
            </div>
          </div>
        @endif


          @if($status=='open')
            <div class="col-md-3">
              <div class="mb-0">
                <form action="/home/take_shopping_list" id="take_list_form_{{ $list->id }}" method="POST">
                  @csrf
                  <button type="submit" class="btn btn-info" name="action" value="{{ $list->id }}">Einkauf Übernehmen</button>
                </form>
              </div>
            </div>
          @endif
          @if($status=='taken')
            <div class="col-md-3">
                <div class="mb-0">
                <button class="btn btn-info" onclick=" window.open('/home/shopping_list_print/{{ $list->id }}','_blank')">Druckansicht</button>
              </div>
            </div>
          @endif
          @if($status=='validate')
            <div class="col-md-3">
                <div class="mb-0">
                <form action="/all/validieren" id="take_list_form_{{ $list->id }}" method="POST">
                  @csrf
                  <button type="submit" class="btn btn-info" name="action" value="{{ $list->id }}">Validieren</button>
                </form>
              </div>
            </div>
          @endif
          @if($status=='refund')
            <div class="col-md-3">
                <div class="mb-0">
                <form action="/all/auszahlen" id="take_list_form_{{ $list->id }}" method="POST">
                  @csrf
                  <button type="submit" class="btn btn-info" name="action" value="{{ $list->id }}">Ausgezahlt</button>
                </form>
              </div>
            </div>
          @endif
          @if($status=='be_paid')
            <div class="col-md-3">
                <div class="mb-0">
                <form action="/all/zahlungen_erhalten" id="take_list_form_{{ $list->id }}" method="POST">
                  @csrf
                  <input type="number" step="0.01" class="form-control" name="paid" required="" value ='' placeholder="">
                  <button type="submit" class="btn btn-info" name="action" value="{{ $list->id }}">Zahlung Erhalten</button>
                </form>
                <a class="btn btn-info" href="/all/invoice/{{ $list->id }}">Rechnung Download</a>

              </div>
            </div>
          @endif
      </div>

  </div>

  <div id="collapse_{{ $list->id }}" class="collapse" aria-labelledby="heading_{{ $list->id }}" data-parent="#accordion">
    @include('shopping_list.contents', array('list' => $list, 'status' => $status))
  </div>
</div>