@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Ablauf eines Einkaufs (Informationen für Helfende)') }}</div>
                <div class="card-body">
                    <b>Als Helfender bist du frei, dort einzukaufen, wo du möchtest. Wir empfehlen, nach Möglichkeit auch lokales Gewerbe zu unterstützen, die von der Krise betroffen sind. Namentlich bei uns gemeldet haben sich bisher:</b><br>
                        <ol>
                            <li>Käse und Milchprodukte: Gsund und Gluschtig (Verkaufswagen)
                                <ol>Kontaktperson: Fam. Anliker, Gehrenweg 2, 5106 Veltheim, 056 443 06 06</ol>
                                <ol>Der Verkaufswagen steht Freitags 14:00-18:30 am Talbachweg in Schinznach. An anderen Tagen kann Ware nach telefonischer Vereinbarung in Veltheim abgeholt werden.</ol>
                            </li>
                            <li>Fleischwaren: Wernli Metzg, Birr
                                <ol>Kontaktperson: Reini Buchle, Johannisweg 8, 5107 Schinznach, 076 330 48 82</ol>
                                <ol>Ware kann in der Filiale in Birr bezogen werden oder bei Reini Buchle bestellt und am Johannisweg 8 abgeholt werden.</ol>
                            </li>
                        </ol>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
