<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Rechnung</title>

    <style type="text/css">
        @page {
            margin: 0px;
        }
        body {
            margin: 30px;
        }
        * {
            font-family: Verdana, Arial, sans-serif;
        }
        a {
            color: #fff;
            text-decoration: none;
        }
        table {
            font-size: x-small;
        }
        .infodiv {
            font-size: x-small;
            margin: 30px;
        }
        tfoot tr td {
            font-weight: bold;
            font-size: x-small;
        }
        .invoice table {
            margin: 15px;
        }
        .invoice h3 {
            margin-left: 15px;
        }
        .information {
            background-color: #60A7A6;
            color: #FFF;
        }
        .information .logo {
            margin: 5px;
        }
        .information table {
            padding: 10px;
        }
    </style>

</head>
<body>

<div class="information">
    <table width="100%">
        <tr>
            <td align="left" style="width: 40%;">
                <h3>{{ $lists[0]->first_name }} {{ $lists[0]->second_name }}</h3>
                <pre>
{{ $lists[0]->street }}
{{ $lists[0]->zip }} {{ $lists[0]->city }}
<br /><br />
Rechnungsdatum: {{ Carbon\Carbon::now()->add(new DateInterval('PT1H'))->format('d.m.Y') }}
{{-- Identifier: #uniquehash --}}
Zu bezahlen auf Rechnung
Zahlungsfrist: 20 Tage
</pre>


            </td>
            <td align="center">
                {{-- <img src="/path/to/logo.png" alt="Logo" width="64" class="logo"/> --}}
            </td>
            <td align="right" style="width: 40%;">

                <h3>Tickit.ch Einkaufshilfe</h3>
                <pre>
                    Manuel Meier
                    Johannisweg 9
                    5107 Schinznach-Dorf
                    076 769 31 00

                    IBAN CH87 8080 8006 7123 3936 8
                </pre>
            </td>
        </tr>

    </table>
</div>


<br/>

<div class="invoice">
    <h3>Sammelrechnung Einkaufshilfe</h3>
    <table width="100%">
        <thead>
        <tr>
            <th>Beschreibung</th>
            <th align="left">Anzahl</th>
            <th align="right">Warenwert CHF</th>
        </tr>
        </thead>
        <tbody>
        @php
            $sum = 0;
        @endphp
        @foreach($lists as $l)
            @php
                $sum += $l->cost;
            @endphp
        <tr>
            <td><b>Einkauf</b> <br>Bestellung: {{ $l->created_at->add(new DateInterval('PT1H'))->format('d.m.Y H:i') }}<br> Auslieferung: {{ $l->delivered->add(new DateInterval('PT1H'))->format('d.m.Y H:i') }}<br> HelferIn: {{ $l->user->name }} {{ $l->user->second_name }}</td>
            <td align="left">1</td>
            <td align="right">{{ number_format($l->cost, 2) }}</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>


        @endforeach
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        </tbody>

        <tfoot>
        <tr>
            <td colspan="1"></td>
            <td align="left">Total</td>
            <td align="right" class="gray">CHF {{ number_format($sum, 2) }}</td>
        </tr>
        </tfoot>
    </table>
</div>
<br>
<br>
<br>
<div class="infodiv">
Hinweis: Unser Service ist kostenlos, Sie bezahlen nur die Ware gemäss Kassenzettel. Trinkgelder werden keine erwartet. Allfällig aufgerundete Beträge werden unter den Helfern aufgeteilt.<br><br><br><br>
Bitte verlassen Sie während der Corona-Epidemie ihr Zuhause nicht extra, um diese Rechnung zu bezahlen. Falls Sie die Zahlungsfrist deshalb nicht einhalten können, dürfen Sie sich gerne telefonisch (076 769 31 00) melden.

</div>
<div class="information" style="position: absolute; bottom: 0;">
    <table width="100%">
        <tr>
            <td align="left" style="width: 50%;">
                &copy; {{ date('Y') }} {{ config('app.url') }} - All Rechte vorbehalten.
            </td>
            <td align="right" style="width: 50%;">
                Tickit.ch - Einkaufshilfe
            </td>
        </tr>

    </table>
</div>
</body>
</html>