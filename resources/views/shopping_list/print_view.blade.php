@extends('layouts.app_bare')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12 mt-3">
          <div class="card">
            <div class="card-header" id="headingOne">
                <div class="row">
                    <div class="col-md-5">
                      <h5 class="mb-0">
                          {{ $list->zip }} {{ str_limit($list->city, 15) }},  {{ str_limit(preg_replace('/[0-9,]+/', '', $list->street), 15) }} 
                      </h5>
                    </div>
                    <div class="col-md-3">
                        <div class="mb-0">

                          Eingang: {{ $list->created_at->add(new DateInterval('PT1H'))->format('d.m.Y H:m') }} 
                      </div>
                    </div>
                </div>

            </div>

            <div class="row">
              @include('shopping_list.contents', array('list' => $list, 'status' => 'print'))
            </div>
          </div>
      </div>
    </div>
</div>
@endsection
